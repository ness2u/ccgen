﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CcGen
{
    class Program
    {
        static void Main(string[] args)
        {            
            int length = 16;
            string cardType = "visa";
            if (args.Length > 0)
            {
                cardType = args[0];
                if (args.Length > 1 && !int.TryParse(args[1], out length))
                {
                    Console.WriteLine("Invalid length, using default of 16.");
                    length = 16;
                }
            }
            else
            {
                Console.WriteLine("Usage: ccgen [ccType] [length]");
                Console.WriteLine("Using defaults.");
            }

            CreditCardGenerator.CreditCard ccType;
            if (!Enum.TryParse(cardType, true, out ccType))
            {
                Console.WriteLine("Invalid credit card type: {0}", cardType);
                Console.WriteLine("Valid credit card types:");
                foreach (var cc in Enum.GetValues(typeof(CreditCardGenerator.CreditCard)))
                {
                    Console.WriteLine(cc);
                }
                return;
            }

            var generator = new CreditCardGenerator();
            var ccNumber = generator.Generate(ccType, length);

            Console.WriteLine(ccNumber);
        }
    }
}

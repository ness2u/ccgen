﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CcGen
{
    public class CreditCardGenerator
    {
        public enum CreditCard
        {
            Amex,
            DinersClub,
            Discover,
            EnRoute,
            Jcb,
            Mastercard,
            Visa,
            Voyager
        }

        private static readonly Dictionary<CreditCard, string[]> Prefixes = new Dictionary<CreditCard, string[]>();
        private static readonly Random Random = new Random();

        static CreditCardGenerator()
        {
            Prefixes.Add(CreditCard.Amex, new[] { "34", "37" });
            Prefixes.Add(CreditCard.DinersClub, new[] { "300", "301", "302", "303", "36", "38" });
            Prefixes.Add(CreditCard.Discover, new[] { "6011" });
            Prefixes.Add(CreditCard.EnRoute, new[] { "2014", "2149" });
            Prefixes.Add(CreditCard.Jcb, new[] { "35" });
            Prefixes.Add(CreditCard.Mastercard, new[] { "51", "52", "53", "54", "55" });
            Prefixes.Add(CreditCard.Visa, new[] { "4539", "4556", "4916", "4532", "4929", "40240071", "4485", "4716", "4" });
            Prefixes.Add(CreditCard.Voyager, new[] { "8699" });
        }

        private static readonly Regex Strip = new Regex(@"\D");

        public bool LuhnCheck(string number)
        {
            if (string.IsNullOrEmpty(number)) { return false; }
            number = Strip.Replace(number, string.Empty);

            int luhn = CalcLuhnSum(number, true);
            return (luhn % 10 == 0);
        }

        public string Generate(CreditCard card, int length = 16)
        {
            if (length < 8 || length > 24) { throw new ArgumentOutOfRangeException(); }

            string prefix = GetPrefix(card);
            string body = RandomNumber(length - 1 - prefix.Length);
            string number = ConstructNumber(prefix, body);

            if (!LuhnCheck(number)) { throw new Exception("Jon's algorithm failed, try again."); }
            return number;
        }

        #region Private Helpers
        private string ConstructNumber(string prefix, string body)
        {
            string raw = prefix + body;
            int luhn = (CalcLuhnSum(raw, false) * 9) % 10;

            return raw + luhn;
        }

        private string RandomNumber(int digits)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < digits; i++)
            {
                int digit = Random.Next() % 10;
                sb.Append(digit.ToString());
            }
            return sb.ToString();
        }

        private string GetPrefix(CreditCard card)
        {
            string[] prefixes;
            if (!Prefixes.TryGetValue(card, out prefixes)) { throw new ArgumentOutOfRangeException("No prefixes for " + card); }

            return prefixes[Random.Next() % prefixes.Length];
        }

        private int CalcLuhnSum(string cc, bool hasChecksum)
        {
            int mod = hasChecksum ? 1 : 0;

            int sum = 0;
            for (int i = 0; i < cc.Length; i++)
            {
                int digit = int.Parse(cc[cc.Length - 1 - i].ToString());
                if (i % 2 == mod)
                {
                    digit *= 2;
                    if (digit >= 10) { digit -= 9; }
                }
                sum += digit;
            }

            return sum;
        }
        #endregion
    }
}

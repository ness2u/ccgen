# CcGen

Generates a credit card number which will pass Luhn test.
Usage: ccgen [ccType] [length]

```
> CcGen.exe
4929142986683514
```

```
> CcGen.exe Amex 15
341663042586811
```

## ccTypes
- Amex
- DinersClub
- Discover
- EnRoute
- Jcb
- Mastercard
- Visa
- Voyager

## length
- Must be between 8 and 24 digits.